# GprinterSDK

#### Description
本项目为使用微信小程序通过l蓝牙连接佳博票据/标签打印机。
实现打印标签和票据的目的
此项目为一个基础Demo，相关个性化功能可以在此基础上进行自行定制。
根据当前测试情况，可以在“佳博 GP-58MBIII”顺利打印，其他型号的打印机尚未测试
有相关需求的小伙伴欢迎来扰

#### Software Architecture
Software architecture description

#### Installation

1.  xxxx
2.  xxxx
3.  xxxx

#### Instructions

1.  xxxx
2.  xxxx
3.  xxxx

#### Contribution

1.  Fork the repository
2.  Create Feat_xxx branch
3.  Commit your code
4.  Create Pull Request


#### Gitee Feature

1.  You can use Readme\_XXX.md to support different languages, such as Readme\_en.md, Readme\_zh.md
2.  Gitee blog [blog.gitee.com](https://blog.gitee.com)
3.  Explore open source project [https://gitee.com/explore](https://gitee.com/explore)
4.  The most valuable open source project [GVP](https://gitee.com/gvp)
5.  The manual of Gitee [https://gitee.com/help](https://gitee.com/help)
6.  The most popular members  [https://gitee.com/gitee-stars/](https://gitee.com/gitee-stars/)
